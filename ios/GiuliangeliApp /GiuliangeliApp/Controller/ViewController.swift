//
//  ViewController.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/11/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
   
    @IBOutlet weak var Username: UITextField!
    
    @IBOutlet weak var Password: UITextField!
    
    @IBOutlet weak var Accedi: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
   

    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
            let isLoggedIn = AuthService.instance.isLoggedIn
            if (isLoggedIn) {
            ContractsService.instance.GetAllContracts(){
               (success) in
                if success {
                self.performSegue(withIdentifier: "ToHomeVC", sender: nil)
                }
            }
        }
    }
      

    
    @IBAction func SignUpBtn(_ sender: Any) {
        
        performSegue(withIdentifier: "ToRegistrationVC", sender: nil)
     
            
    }
    @IBAction func LoginBtn(_ sender: Any) {
  
        guard let password = Password.text, Password.text != nil  && Password.text != "" else
        {
            return
            
        }
        guard let email = Username.text, Username.text != nil && Username.text != "" else {
            return
            
        }
        
        AuthService.instance.loginUser(email: email, password: password){
        (success) in
         if success {
          ContractsService.instance.GetAllContracts(){
                   (success) in
                     if success {
                        self.performSegue(withIdentifier: "ToHomeVC", sender: nil)
                     }
            
        }
           
          
         } else {
            print("logimi deshtoi") }
                            
        }
    }
    
}


