//
//  DetailContractVC.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/16/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit
import Photos
class DetailContractVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
  

    @IBOutlet weak var collectionView: UICollectionView!
    
    
    @IBOutlet weak var codeLbl: UILabel!
    
    @IBOutlet weak var consegna: UILabel!
    @IBOutlet weak var rientro: UILabel!
    
    @IBOutlet weak var cliente: UILabel!
    
    @IBOutlet weak var targa: UILabel!
    
    @IBOutlet weak var km: UILabel!
    

    
    //index
    var Index : Int!
    var id : String!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let contract = ContractsService.instance.Contracts[Index]

        codeLbl.text = contract.code
        consegna.text = contract.data_consegna
        rientro.text = contract.data_rientro
        cliente.text = contract.nome_cliente + " " + contract.cognome_cliente
        targa.text = contract.targa
        km.text = contract.km
        
        id = contract.id
    
        
        //per fshirjen tek PopupVC
  NotificationCenter.default.addObserver(self,selector: #selector(reloadCollectionViewFromPopupVC),name : NSNotification.Name(rawValue: "reloadAfterPopupVCDeletion"), object: nil )
    }
     //funksion per reload me ane te NotificationCenter
      @objc func reloadCollectionViewFromPopupVC(notification : NSNotification){
        self.collectionView.reloadData()
      }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.reloadData()

    }
    
    
    func getClient(index: Int) {
        
      Index = index

        }
    

    @IBAction func back(_ sender: Any) {
       
         _ = navigationController?.popViewController(animated: true)
    }
    
        
  
    
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var itemnr : Int = 0
        if DownloadImageService.instance.ImageDataArray.count != 0 {itemnr = DownloadImageService.instance.ImageDataArray.count}
          if DownloadImageService.instance.ImageKeyValueArray.count != 0 {itemnr = DownloadImageService.instance.ImageKeyValueArray.count}
        return itemnr
            
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0
        let yourHeight = yourWidth + 40

        return CGSize(width: yourWidth, height: yourHeight)
    }
     //margins per item ne qelize
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsets.zero
      }
   //hapsira midis qelizave
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
    
         //spacing between successive rows or columns of a section
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return 0
      }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? CollectionViewImageCell{

            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetailContractVC.tap(_ :)))
               cell.image.isUserInteractionEnabled = true
               cell.image.tag = indexPath.row
               cell.image.addGestureRecognizer(tapGestureRecognizer)
            
               cell.updateCell(indexPath: indexPath.row)
                  cell.CellDelegate = self
                  cell.index = indexPath
                   return cell

                }
        else {
            return CollectionViewImageCell()
           
              }

        }
    
    
    
        var imageIndex : Int!
    @objc func tap(_ sender : AnyObject ){
        imageIndex = sender.view.tag
        DeleteServic.instance.Id = id
        DeleteServic.instance.ImgIndex = String(sender.view.tag)
        performSegue(withIdentifier: "ToImageDetail", sender: sender.view.tag)
        
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ImageDetailVC = segue.destination as? ImageDetailVC {
            ImageDetailVC.getImageIndex(ImageIndex: imageIndex)
        }
    }
    
    @IBAction func logoutBtn(_ sender: Any) {
        
        
           AuthService.instance.isLoggedIn = false
             AuthService.instance.authToken = "token"
            
 
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        
    }
    
    
    @IBAction func UploadImageBtnClick(_ sender: Any) {

        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
          
           let imagePicker = UIImagePickerController()
           imagePicker.delegate = self
           imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
          
            
            PHPhotoLibrary.requestAuthorization { (PHAuthorizationStatus) in
                 switch PHAuthorizationStatus {
                           case .authorized:
                            DispatchQueue.main.async {
                             self.present(imagePicker, animated: true, completion: nil)
                                
                            }
                               
                           case .denied:
                               print("Access Denied")
                               
                           case .notDetermined:

                           PHPhotoLibrary.requestAuthorization({ (newStatus) in
                            if newStatus == .authorized {
                                DispatchQueue.main.async {
                                self.present(imagePicker, animated: true, completion: nil)
                                   
                                } }
                              
                              })

                          case .restricted:
                               print("Restricted")
                           @unknown default:
                               print("fatal error")
                           }
                
            }

   
        }

    }
 
}


extension DetailContractVC : BtnCell {
    func OnClickCell(index: Int) {
     
    
        //tek sherbimi ndertuam dyvariabla dhe u dhame vlere ketu qe kur te shkojme tek popup te jene te setuar
        DeleteServic.instance.ImgIndex = String(index)
        DeleteServic.instance.Id = id
        
        let popup = PopupVC()
        popup.modalPresentationStyle = .custom
        present(popup, animated: true, completion: nil)
        
        
       
    }

}



extension DetailContractVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

           let contract = ContractsService.instance.Contracts[Index]
            let clientId = contract.id
   
              let fileManager = FileManager.default
              let documentsPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
              let imagePath = documentsPath?.appendingPathComponent("image.jpg")
            
        
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                try! pickedImage.jpegData(compressionQuality: 0.0)?.write(to: imagePath!)
                  }
        
        UploadImageService.instance.UploadImage(clientId: clientId, image: imagePath!){
            (success) in
            if success {
                self.collectionView.reloadData()
                self.dismiss(animated: true, completion: nil)
                
            }  else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}

 
