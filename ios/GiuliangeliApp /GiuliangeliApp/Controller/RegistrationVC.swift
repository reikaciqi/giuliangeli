//
//  RegistrationVC.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/11/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit

class RegistrationVC: UIViewController {

    
    @IBOutlet weak var nameTxt: TextFields!

    @IBOutlet weak var emailTxt: TextFields!

    @IBOutlet weak var passwordTxt: TextFields!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func dismissBtn(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)

    }
    
    @IBAction func SignUpBtn(_ sender: Any) {
        
        guard let name = nameTxt.text , nameTxt.text != "" else {
            return
        }
        
        guard let email = emailTxt.text , emailTxt.text != "" else {
            return
        }
        
        guard let password = passwordTxt.text, passwordTxt.text != "" else {
            return
        }
        
        AuthService.instance.registerUser(email: email, password: password, name: name) {
            (success) in
            if success {
                print ("sucessfully registered user")
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
    
}
