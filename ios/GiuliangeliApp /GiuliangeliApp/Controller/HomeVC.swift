//
//  HomeVC.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/15/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit

class HomeVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    
    
    var theIndex : Int!
    @IBOutlet weak var contractsTable: UITableView!
    
    @IBOutlet weak var startDate: UITextField!
    
    @IBOutlet weak var endDate: UITextField!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        endDate.addTarget(self, action: #selector(getClientsByDate), for: .editingDidEnd) 

         contractsTable.dataSource = self
         contractsTable.delegate = self
        
    }
    
    @objc func getClientsByDate(){
        guard let StartDate = startDate.text, startDate.text != "" else{
            return
        }
        guard let EndDate = endDate.text, endDate.text != "" else {
            return
        }
        
        ContractsService.instance.getContracts(startDate: StartDate, endDate: EndDate){
            (success) in
            if success {
                self.contractsTable.reloadData()
            }
           
        }
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ContractsService.instance.Contracts.count
    }
    
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ContractsCell") as?  ContractsCell{
            let contract = ContractsService.instance.Contracts[indexPath.row]
            cell.updateView(contract: contract)
            cell.CellDelegate = self
            cell.index = indexPath
            return cell
        } else {
           return ContractsCell()
            
        }
    }
    
    
  
    
    @IBAction func logoutBtn(_ sender: Any) {
        AuthService.instance.isLoggedIn = false
        AuthService.instance.authToken = "token"
        _ = navigationController?.popToRootViewController(animated: true)
    }

}


extension HomeVC : ContractIndex {
    
    func onClientClickCell(index: Int) {

         theIndex = index
        //ne rast se dikush tjt perdor te dhenat , merren nga fillimi klientet dhe updatohen imazhet
        ContractsService.instance.GetAllContracts(){
            (success) in
            if success {

        let contract = ContractsService.instance.Contracts[self.theIndex]
            DownloadImageService.instance.DownloadImages(contract: contract){
            (success) in
            if success{ self.performSegue(withIdentifier: "ToDetails", sender: self.theIndex)
                               
                           }
                       }
                
            }
        }
         
    }
    
      override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if let detailVC = segue.destination as? DetailContractVC {
            detailVC.getClient(index: theIndex)
          }
      }
    
}
