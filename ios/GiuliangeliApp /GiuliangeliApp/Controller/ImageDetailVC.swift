//
//  ImageDetailVC.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/22/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit

class ImageDetailVC: UIViewController {
    var TheImageIndex : Int!
    
    @IBOutlet weak var image: UIImageView!
    
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let imgIndex = TheImageIndex {
       //  let imageToDisplay = DownloadImageService.instance.ImageArray[imgIndex]
           // image.image = imageToDisplay
            
                   if DownloadImageService.instance.ImageDataArray.count != 0 {
                        let ImageDataArray = DownloadImageService.instance.ImageDataArray
                        let MyImageUrl = DOWNLOAD_IMAGE_URL + ImageDataArray[imgIndex]
                 print("url e imazhit te kerkuar per te pare: \(MyImageUrl)")
                        let url = URL(string: MyImageUrl)
                         image.kf.setImage(with: url)
                        } else
                  if DownloadImageService.instance.ImageKeyValueArray.count != 0 {
                        let ImageKeyValueArray = DownloadImageService.instance.ImageKeyValueArray
                        let MyImageUrl = DOWNLOAD_IMAGE_URL + ImageKeyValueArray[imgIndex].value
                        print("url e imazhit te kerkuar per te pare: \(MyImageUrl)")
                        let url = URL(string: MyImageUrl)
                        image.kf.setImage(with: url)
                             
             }
         }
     }

    func getImageIndex(ImageIndex : Int){
        TheImageIndex = ImageIndex
       
    }
    

    
    @IBAction func Indietro(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Elimina(_ sender: Any) {
        DeleteServic.instance.deleteImage(){
            (success) in
            if success {
                
                _ = self.navigationController?.popViewController(animated: true)
            }
            else {
                print("imazhi nuk u fshi")
            }
        }
        
    }

}
