//
//  TextFields.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/11/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit
@IBDesignable
class TextFields: UITextField {

    override func prepareForInterfaceBuilder() {
        customizeTextFields()    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        customizeTextFields()    }
   
    
    func customizeTextFields(){
        if let p = placeholder {
            
            let place = NSAttributedString(string: p , attributes: [.foregroundColor : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)] )
            attributedPlaceholder = place
            textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        layer.borderWidth = 1.5
        layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        layer.cornerRadius = 8    }

}

