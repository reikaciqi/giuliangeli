//
//  ContractsCell.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/15/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit

protocol ContractIndex {
    func onClientClickCell(index: Int)
}

class ContractsCell: UITableViewCell {

    var CellDelegate : ContractIndex?
    var index: IndexPath?
    
    
    
    @IBOutlet weak var code: UILabel!
    
    @IBOutlet weak var consegna: UILabel!
    
    @IBOutlet weak var rientro: UILabel!
    
    @IBOutlet weak var nome: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func updateView(contract: Contract){
        
        self.code.text = contract.code
        self.consegna.text = contract.data_consegna
        self.rientro.text = contract.data_rientro
        let _nome = contract.nome_cliente + " " + contract.cognome_cliente
        self.nome.text = _nome
        
    }

    @IBAction func OnContractBtnClick(_ sender: Any) {
        CellDelegate?.onClientClickCell(index: (index?.row)!)
    }
}
