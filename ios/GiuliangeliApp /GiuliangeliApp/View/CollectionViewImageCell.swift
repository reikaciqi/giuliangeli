//
//  CollectionViewImageCell.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/16/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit

protocol BtnCell {
    func OnClickCell(index: Int)
}

class CollectionViewImageCell: UICollectionViewCell {
    
   
    @IBOutlet weak var image: UIImageView!
    
   
    var CellDelegate : BtnCell?
    var index : IndexPath?
    

    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
  
    
    
    func updateCell(indexPath: Int){
     
         if DownloadImageService.instance.ImageDataArray.count != 0 {
            let ImageDataArray = DownloadImageService.instance.ImageDataArray
            let MyImageUrl = DOWNLOAD_IMAGE_URL + ImageDataArray[indexPath]
            let url = URL(string: MyImageUrl)
            image.kf.setImage(with: url)
            
     } else
         if DownloadImageService.instance.ImageKeyValueArray.count != 0 {
            let ImageKeyValueArray = DownloadImageService.instance.ImageKeyValueArray
            let MyImageUrl = DOWNLOAD_IMAGE_URL + ImageKeyValueArray[indexPath].value
            let url = URL(string: MyImageUrl)
            image.kf.setImage(with: url)
            
            
           
                 }
    }
    
    @IBAction func OnEliminaBtnClick(_ sender: Any) {
        CellDelegate?.OnClickCell(index: (index?.row)!)
    }
    
    
    //per popupVc
    
    
    
 
}
   
