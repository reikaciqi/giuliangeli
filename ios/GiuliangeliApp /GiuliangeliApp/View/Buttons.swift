//
//  Buttons.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/14/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit

@IBDesignable
class Buttons: UIButton {

    override func prepareForInterfaceBuilder() {
        layer.cornerRadius = 8     }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 8    }
    
    
    
    
}
