//
//  Constants.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/14/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
//nje closure per te pare nese kerkesa e bere ka qene e suksesshme
typealias CompletionHandler = (_ Success: Bool) -> ()


let TOKEN_KEY = "token"
let LOGGED_IN_KEY = "loggedIn"




//URL
let LOGIN_URL : String = "http://api-task.draft2017.com/api/auth/login"
let SIGNUP_URL  : String = "http://api-task.draft2017.com/api/auth/signup"
let CONTRACTS_URL :String = "http://api-task.draft2017.com/api/clienti"
var UPLOAD_IMG_URL : String = "http://api-task.draft2017.com/api/upload/"
var DELETE_IMG_URL :String = "http://api-task.draft2017.com/api/remove/"
var DOWNLOAD_IMAGE_URL : String = "http://api-task.draft2017.com"
