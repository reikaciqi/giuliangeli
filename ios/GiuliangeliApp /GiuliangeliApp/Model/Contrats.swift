//
//  Contrats.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/15/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation

struct Contract {
    private(set) public var id : String
    private(set) public var code : String
    private(set) public var data_consegna: String
    private(set) public var data_rientro : String
    private(set) public var nome_cliente : String
    private(set) public var cognome_cliente : String
    private(set) public var km : String
    private(set) public var targa : String
    private(set) public var immagini : String
    
    
    init(id: Int, code:String, data_consegna:String, data_rientro:String,nome_cliente:String, cognome_cliente: String, km:String, targa: String, immagini: String){
        self.id = String(id)
        self.code = code
        self.data_consegna = data_consegna
        self.data_rientro = data_rientro
        self.nome_cliente = nome_cliente
        self.cognome_cliente = cognome_cliente
        self.km = km
        self.targa = targa
        self.immagini = immagini
        
    }
    
    
    
    
}
