//
//  ClientImages.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/21/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
import UIKit

struct ClientImage {
    private(set) public var Key : String
    private(set) public var value : String
    
    
    
    init(key : String, value: String){
        self.Key = key
        self.value = value
    }
    
    
    
}
