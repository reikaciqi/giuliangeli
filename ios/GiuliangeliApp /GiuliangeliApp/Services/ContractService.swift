//
//  ContractService.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/15/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
import Alamofire



class ContractsService{
    static let instance = ContractsService()
    
   
   var Contracts = [Contract]()
    
    
    func getContracts(startDate:String, endDate:String, completion: @escaping CompletionHandler){
      
        if Contracts.count != 0 {
            Contracts.removeAll()
        }
        
        let token = "Bearer \(AuthService.instance.authToken)"
   
        let header : HTTPHeaders = [
            "Authorization": token
           
        ]
        
        let param = [
            "startDate" : startDate,
            "endDate" : endDate
        ]
        
        
        AF.request(CONTRACTS_URL , method: .get, parameters: param, encoder: URLEncodedFormParameterEncoder.default, headers: header).responseJSON{
              response in
                      switch response.result {
                             case .success(let val):
                             
                                  if let json = val as? Dictionary<String, Any>{
                                    if let message = json["message"] as? String { print(message) }
                                    
                                    if let clientsJson = json["clients"] as? [[String:Any]] {
                                        for item in clientsJson {
                                            let id = item["id"] as! Int
                                            let code = item["code"] as! String
                                            let data_consegna = item["data_consegna"] as! String
                                            let data_rientro = item["data_rientro"] as! String
                                            let nome_cliente = item["nome_cliente"] as! String
                                            let cognome_cliente = item["cognome_cliente"] as! String
                                            let km = item["km"] as! String
                                            let targa = item["targa"] as! String
                                            let immagini = item["immagini"] as! String
                                            
                                            let contract = Contract(id: id , code: code , data_consegna: data_consegna , data_rientro: data_rientro , nome_cliente: nome_cliente , cognome_cliente: cognome_cliente , km: km, targa: targa, immagini: immagini)
                                            self.Contracts.append(contract)
                                        }
                                        
                                    }
                                  }
                                
                                 completion(true)
                                 
                             case .failure(let error):
                                 print(error)
                                 completion(false)
                             }
                              
                        }
    }

    
    func GetAllContracts(completion: @escaping CompletionHandler){
            if Contracts.count != 0 {
                 Contracts.removeAll()
             }
             
             let token = "Bearer \(AuthService.instance.authToken)"
        
             let header : HTTPHeaders = [
                 "Authorization": token
                
             ]
        
        
             AF.request(CONTRACTS_URL , method: .get,  headers: header).responseJSON{
                 response in
                         switch response.result {
                                case .success(let val):
                                
                                     if let json = val as? Dictionary<String, Any>{
                                       if let message = json["message"] as? String { print(message) }
                                       
                                       if let clientsJson = json["clients"] as? [[String:Any]] {
                                           for item in clientsJson {
                                               let id = item["id"] as! Int
                                               let code = item["code"] as! String
                                               let data_consegna = item["data_consegna"] as! String
                                               let data_rientro = item["data_rientro"] as! String
                                               let nome_cliente = item["nome_cliente"] as! String
                                               let cognome_cliente = item["cognome_cliente"] as! String
                                               let km = item["km"] as! String
                                               let targa = item["targa"] as! String
                                               let immagini = item["immagini"] as! String
                                               
                                               let contract = Contract(id: id , code: code , data_consegna: data_consegna , data_rientro: data_rientro , nome_cliente: nome_cliente , cognome_cliente: cognome_cliente , km: km, targa: targa, immagini: immagini)
                                               self.Contracts.append(contract)
                                          }
                                           
                                       }
                                     }
                                   
                                    completion(true)
                                    
                                case .failure(let error):
                                    print(error)
                                    completion(false)
                                }
                                 
                           }

    }
    
    
}
