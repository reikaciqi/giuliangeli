//
//  DeleteImageService.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/17/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
import Alamofire

class DeleteServic{
    
    static let instance = DeleteServic()
    
    var  ImgIndex : String!
    var Id : String!

    let header : HTTPHeaders =
    [
      "Authorization": "Bearer \(AuthService.instance.authToken)"
                     
    ]
    
    
    
    
    
    func deleteImage(  completion: @escaping CompletionHandler){

        
        if DownloadImageService.instance.ImageDataArray.count != 0 {
            
            deleteRequest(imgIdentifier: ImgIndex){
                (success) in
                if success { completion(true)  }
            }
         }
        else if DownloadImageService.instance.ImageKeyValueArray.count != 0
        {
            let imgKey = DownloadImageService.instance.ImageKeyValueArray[Int(ImgIndex)!].Key
            deleteRequest(imgIdentifier: imgKey){
                (success) in
                if success { completion(true)  }
            }
        }
  }
    
    
    func deleteRequest(imgIdentifier : String , completion: @escaping CompletionHandler){
        
         AF.request(DELETE_IMG_URL + Id + "/" + imgIdentifier, method: .delete, headers: header).responseJSON{
            response in
            switch response.result {
                             
                   case .success(let val):
                   print("key i fshirjes \(imgIdentifier)")
                               
                   UpdateAfterChange.instance.UpdateData(clientId: self.Id){
                    (success) in
                     if success{
                        print("te dhenat u moren pas fshirjes")
                        completion(true)
                        
                       }
                     }
                   
                     print(val)
                             
           
                   case .failure(let error):
                   print(error)
                                 
                 }
             }

         }
 
}
