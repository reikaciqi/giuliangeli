//
//  DownloadImageService.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/18/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Kingfisher


class DownloadImageService {
    static let instance = DownloadImageService()
    
     var ImageArray = [UIImage]()
    
    //krijohet tabela bosh qe do mbaje vlerat qe vijne nga stringu i imazhit
    //ne rastin kur te dhenat vijne ne formatin array string
    var ImageDataArray = [String]()
   

    //ne rast se vjen en formatin dictionary
    var ImageKeyValueArray = [ClientImage]()

    

    func DownloadImages(contract: Contract, completion: @escaping CompletionHandler){
        
        if ImageDataArray.count != 0 {
            ImageDataArray.removeAll()
        }
        
        if ImageArray.count != 0 {
            ImageArray.removeAll()
        }
        
        if ImageKeyValueArray.count != 0 {
            ImageKeyValueArray.removeAll()
        }
        
  
      let jsonString = contract.immagini
      let data = jsonString.data(using: .utf8)!
        
        
        
        do {
            
            if  let ImgArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String]
            {
                
            ImageDataArray = ImgArray
            completion(true)
                
            } else {

                do {
                    if let jsonDictionary = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String : String] {
                      
                     for (key, value) in jsonDictionary{
                        let image = ClientImage(key: key, value: value)
                        ImageKeyValueArray.append(image)
                       
                       }

                        completion(true)
                        
                    } else {
                        completion(false)
                    }
                    
                } catch let error as NSError {
                    print(error)
                }
 
            }
            
        } catch let error as NSError {
            print(error)
        }

    }

    

}
