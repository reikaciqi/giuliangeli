//
//  AuthService.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/11/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
import Alamofire

class AuthService{
    static let instance = AuthService()
    
    let defaults = UserDefaults.standard
    var isLoggedIn : Bool {
        get {
        return defaults.bool(forKey: LOGGED_IN_KEY)
    }
    set {
        defaults.set(newValue, forKey: LOGGED_IN_KEY)
    }
}
    
    
    
    var authToken: String {
        get {
            return defaults.value(forKey: TOKEN_KEY) as! String
        }
        
        set {
            defaults.set(newValue, forKey: TOKEN_KEY)
        }
    }
            

    
    
            //funksioni qe ben regjistrimin e nje useri
    func registerUser(email: String, password: String, name: String, completion: @escaping CompletionHandler){
        let lowerCaseEmail = email.lowercased()
        
     
        
     let registrationParameters = [
        "email" : lowerCaseEmail,
        "password" : password,
        "name": name
        ]
        
        AF.request(SIGNUP_URL, method: .post, parameters: registrationParameters).responseJSON{
            response in
            switch response.result {
            case .success(let val):
                if let json = val as? Dictionary<String , Any>{
                if let message = json["message"] as? String {print(message)}
                }
                completion(true)
                
            case .failure(let error):
                print(error)
                completion(false)
            }

        }

    }
    
    //funksioni qe ben logimin e nje useri
    func loginUser(email:String, password:String, completion: @escaping CompletionHandler){
     let param = [
        "email" : email,
        "password" : password
        ]
        
        AF.request(LOGIN_URL ,method: .post, parameters: param).responseJSON{
            response in
        switch response.result {
        case .success(let val):
         
            if let json = val as? Dictionary<String, Any>{
                if let token = json["access_token"] as? String { self.authToken = token  }
                if let id_user = json["user_id"] as? Int { print("Id e userit: \(id_user)")  }
                self.isLoggedIn  = true
         
                completion(true)
                
            }

        case .failure(let error):
            print(error)
            completion(false)
        }
           
        }
    }
    
    
 
 
    
        
}

