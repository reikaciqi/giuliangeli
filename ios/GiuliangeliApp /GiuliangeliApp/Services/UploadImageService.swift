//
//  UploadImageService.swift
//  GiuliangeliApp
//
//  Created by Rei on 10/17/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
class UploadImageService {
static let instance = UploadImageService()
    
    func UploadImage(clientId: String, image: URL, completion: @escaping CompletionHandler){
        
        let token = "Bearer \(AuthService.instance.authToken)"
   

       
        let headers : HTTPHeaders = [
                "Authorization": token,
                "Content-Type": "application/x-www-form-urlencoded"   ,
                "Accept" : "application/json"
        ]
      
      
               
        let param : Parameters = [
            "immagine": image
             ]
 
        
        AF.upload(multipartFormData: { multipartFormData in
           
            for (key, value) in param {
                multipartFormData.append(value as! URL, withName : key)
            }
        }, to: UPLOAD_IMG_URL + clientId , method: .post, headers: headers)
            .responseJSON { response in
                debugPrint(response)
                UpdateAfterChange.instance.UpdateData(clientId: clientId){ 
                    (success) in
                    if success {
                        completion(true)
                    }
                }
           }
      }
}
